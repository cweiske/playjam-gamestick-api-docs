******************************************
PlayJam GameStick server API documentation
******************************************

This is an attempt to document the network API used by the PlayJam GameStick
gaming console.

.. contents::
   :depth: 2


API basics
**********
.. include:: api/use-cases.rst
.. include:: api/data.rst
.. include:: api/headers.rst


Uncategorized
*************
.. include:: api/ping.rst
.. include:: api/connectcheck.gamestickservices.net/generate_204.rst
.. include:: api/db.gamestickservices.com/api-rest-joystick-bindings-xxx.rst
.. include:: api/dev-db.gamestickservices.net/api-rest-developer-validate-xxx-yyy.rst
.. include:: api/l2.gamestickservices.net/api-rest-connect-stick-stick-xxx.rst
.. include:: api/l2.gamestickservices.net/api-rest-parentcontroll-change-agerating-xxx-yyy.rst
.. include:: api/l2.gamestickservices.net/api-rest-player-profile.rst
.. include:: api/update.gamestickservices.net/check.php.rst
.. include:: api/www.playjam.com/bundles-system-meta.json.rst


Achievements
************
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-achievements.rst
.. include:: api/l2.gamestickservices.net/api-rest-user-achievement-summary.rst
.. include:: api/l2.gamestickservices.net/api-rest-user-achievement-xxx-set-complete.rst
.. include:: api/l2.gamestickservices.net/api-rest-user-game-xxx-achievement-list.rst


Analytics
*********
.. include:: api/l2.gamestickservices.net/api-rest-analytics-application-event-analytics-event.rst
.. include:: api/l2.gamestickservices.net/api-rest-game-downloadedfreegame-true.rst
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-event-end-game.rst
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-event-start-game.rst


Leaderboards
************
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-leadrboard-top50.rst
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-save-score-yyy-extend.rst


Payments
********
.. include:: api/l2.gamestickservices.net/api-rest-wallet-payment-game-xxx-init-transaction-do.rst
.. include:: api/l2.gamestickservices.net/api-rest-wallet-payment-topupwallet-init-transaction-do.rst


Savegames
*********
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-save-state.rst
.. include:: api/l2.gamestickservices.net/api-rest-game-xxx-state.rst


About
*****
This documentation has been written by Christian Weiske,
cweiske+ouya@cweiske.de.

Last update: ##UPDATE##


=======
License
=======
It is licensed under the GNU Free Documentation License.


===========
Source code
===========
The documentation sources are available at
https://git.cweiske.de/playjam-gamestick-api-docs.git
and mirrored at
https://codeberg.org/cweiske/playjam-gamestick-api-docs


=========
Home page
=========
A rendered version of this documentation is available at
http://cweiske.de/gamestick-api-docs.htm


========
Building
========
You need to install ``rst2html5`` before::

    $ pip install rst2html5-tools

Rendering the docs is done via a build script::

    $ ./build.sh
