=========
Use cases
=========

Setup
=====
A new PlayJam GameStick does the following requests:

FIXME verify

OOBE (Out Of Box Experience) process starts with the GameStick setup:

1. Fetch registration code and show it on screen: `POST http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json`_
2. Check every 5 seconds if the online registration has been finished: also `POST http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json`_
3. Download game metadata: also `POST http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json`_
