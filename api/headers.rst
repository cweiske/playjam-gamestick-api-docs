============
HTTP Headers
============


``User-Agent``
==============
The user agent may have two different values:

- ``Dalvik/1.6.0 (Linux; U; Android 4.1.2; GameStick V1.0 Build/V1.03.04MX01_20130911)``
- ``GameStick/1.0``


``GameStick/1.0``
-----------------
Is used when the HTTP request is coming from ``com.playjam.EnhancedDownloadService``.

The ``Accept-Encoding`` header is ``identity`` in that case.
