=========================
``ping`` connection check
=========================

The connection check in ``com.playjam.gamestick.WifiTools.ChecksFragment#ping``
sends ICMP ``ping`` requests to three domains to see if the network is available:

- ``connectcheck.gamestickservices.net``
- ``l2.gamestickservices.net``
- ``update.gamestickservices.net``
