==================================================================================================
``GET http://l2.gamestickservices.net/api/rest/parentcontroll/change/agerating/xxx/yyy/view.json``
==================================================================================================

Change the profile's minAge setting.

Only games suitable for that age are shown by the GameStick Console UI.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/parentcontroll/change/agerating/xxx/yyy/view.json;jsessionid=zzz``

  ``xxx``
    Age rating:

    - 3
    - 7
    - 12
    - 17
  ``yyy``
    MD5-hashed user password
  ``zzz``
    Session ID


HTTP response
=============

Successful change
-----------------
Status code
  ``200 OK``

.. include:: api-rest-parentcontroll-change-agerating-xxx-yyy.response-success.json
   :code:

``action`` and ``message`` do not seem to be needed.


Wrong password
--------------
Status code
  ``200 OK``

.. include:: api-rest-parentcontroll-change-agerating-xxx-yyy.response-wrongpass.json
   :code:

Neither ``action`` no ``message`` seem to be needed.
