====================================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/event/start-game/view.json``
====================================================================================

Notify the server that a game has been started.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/event/start-game/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``zzz``
    Session ID


HTTP response
=============
Must be on a single line.

Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-event-start-game.response.json
   :code:
