============================================================================================
``GET http://l2.gamestickservices.net/api/rest/user/achievement/xxx/set-complete/view.json``
============================================================================================

When some achievement has been reached/completed in a game.

Achievements had to be registered at PlayJam.

HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/user/achievement/xxx/set-complete/view.json;jsessionid=zzz``

  ``xxx``
    Global achievement ID.

    Example: ``461``
  ``zzz``
    Session ID

Example::

  GET http://l2.gamestickservices.net/api/rest/user/achievement/461/set-complete/view.json;jsessionid=sacdbda09185c


HTTP response
=============

Successful registration
-----------------------
Status code
  ``200 OK``

FIXME: Verify if this is correct

.. include:: api-rest-user-achievement-xxx-set-complete.response-success.json
   :code:
