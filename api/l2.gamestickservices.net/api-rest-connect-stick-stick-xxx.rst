===================================================================================
``POST http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json``
===================================================================================

Used for several things:

1. Network connection check
2. Fetch registration code and session ID for this gamestick.
3. Fetch information about games and their display in the main menu


HTTP request
============
When the GameStick is not registered yet, then this URL is fetched
every 5 seconds.
Once registered, it is fetched every 2 minutes.

The initial request does not contain a session ID.
The hardware ID is used to associate the stick with an existing
user ID and a new session ID is generated.


Method
  ``POST``

  Firmware 0.0.53 uses ``GET``.

  ``DatabaseService.ServiceCore`` can be put into GET mode in Firmware 2071,
  in that case, GET will be used as well.
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``

  Firmware 0.0.53 uses ``db.gamestickservices.net``.
Path
  ``/api/rest/connect/stick/stick/xxx/view.json``

  ``xxx``
    Hardware-ID, e.g. ``ac:db:da:09:18:5c``

  ``;jsessionid=zzz``
    In GET mode, but only after the initial request.
Headers
  ``Accept-Encoding``
    ``identity``
  ``Content-Type``
    ``application/x-www-form-urlencoded``
  ``Content-Length``
    ``0``
  ``User-Agent``
    ``GameStick/1.0`` (when requested via ``com.playjam.EnhancedDownloadService``)
  ``If-Modified-Since``
    When the GameStick cache contains a previous version.

    Example: ``If-Modified-Since: Thu, 06 Jul 2023 07:02:39 UTC``

    Not sent when the Cookie is sent.
POST parameters
  None
Cookies
  ``JSESSIONID``
    Session ID, only when available (not empty)

    In POST mode only
  ``AWSELB``
    When available

    In POST mode only.


HTTP response
=============
When used for connection check, the response must contain
one of the following strings (no whitespace after ``:``)::

  "status":"CONNECTION_IN_PROGRESS"
  "status":"CONNECTED"


Not registered yet - ``CONNECTION_IN_PROGRESS``
-----------------------------------------------
Status code
   ``200 OK``

.. include:: api-rest-connect-stick-stick-xxx.response-unregistered.json
   :code:


Registration complete - ``CONNECTED``
-------------------------------------
Status code
  ``200 OK``

  ``304 Not Modified`` - when the ``If-Modified-Since`` header value is equal
  or higher than the data timestamp.


``body.config.apps[].genre``
  Not used in firmware 2071, but still available - probably for older versions.
``body.config.apps[].genres``
  Known genres:

  - Action
  - Adventure
  - Arcade
  - Classics
  - Media
  - Platformer
  - Puzzle
  - Racing
  - Shmup
  - Shooter

``body.config.apps[].images.name``
  Special name ``STICK_SCREENSHOT`` adds the image URLs as screenshot,
  otherwise as thumbnail. (firmware 2071).

  Known names:

  - ``STICK_ICON`` (85x48)
  - ``STICK_REGISTRATION_GAME_ICON`` (200x200)
  - ``STICK_SCREENSHOT`` (350x160)
  - ``STICK_SCREENSHOT_1080`` (525x240)
  - ``STICK_THUMBNAIL1`` (350x88)
  - ``STICK_THUMBNAIL2`` (350x160)
  - ``STICK_THUMBNAIL3`` (350x236)
  - ``STICK_THUMBNAIL4`` (350x400)
  - ``STICK_VIDEO1_SCREENSHOT`` (350x160)

  ``STICK_SCREENSHOT`` and ``STICK_SCREENSHOT_1080`` may have multiple URLs;
  the other ones only one.

``body.config.global.uitranslation``
  Not used in firmware 2058 and 2071.

``body.config.global.newfeatured.ages[].entries[].columnentries[].thumbnail``
  The code says this is the "tile size".

  - ``6`` for full-height (one game in this column)
    ``STICK_THUMBNAIL4`` is used.
  - ``4`` for 2/3 height (two games in this column, one size 4, one size 2)
    ``STICK_THUMBNAIL3``
  - ``3`` for 1/2 (two games in this column)
    ``STICK_THUMBNAIL2``
  - ``2`` for 1/3 height (3 games in this column)
    ``STICK_THUMBNAIL1``

.. include:: api-rest-connect-stick-stick-xxx.response-registered.json
   :code:



Usage
=====
- ``com.playjam.gamestick.WifiTools.ChecksFragment#doDatabaseConnect()``

  To check if a network connection is available.
  Only uses ``l2.gamestickservices.net``.
- ``com.playjam.DatabaseService.apk``: ``com.playjam.Services.Database.ConnectDownloader``
