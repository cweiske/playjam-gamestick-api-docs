======================================================================================================
``GET http://l2.gamestickservices.net/api/rest/analytics/application-event/analytics/event/view.json``
======================================================================================================

Send user behavior data to the server (tracking).


Known to be used in firmware versions:

- 2058


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/analytics/application-event/analytics/event/view.json;jsessionid=zzz?Map=yyy``

  ``zzz``
    Session ID from the registration check
    ``api/rest/connect/stick/stick/xxx/view.json``.

  ``yyy``
    Actual tracking data::

      {"NAVIGATE":"Games Featured Menu","NAVIGATE":"Media All Menu","NAVIGATE":"Profile"}

    Looks like JSON, but has duplicate keys.

    Known keys:

    - ``NAVIGATE``


HTTP response
=============
Headers
  ``Content-Type``
    ``application/json``

All data in the response must be on one line.

.. include:: api-rest-analytics-application-event-analytics-event.response-full.json
   :code:

If the response is not successful, or the response cannot be parsed into a
JSON object from one line of response data,
the tracking data are sent again some minutes later.


Usage
=====
Analytics intents are sent by the console application.

Response gets parsed in
``com.playjam.gamestick.databaseinterfaceservice.DatabaseInterfaceService#ParseResponse()``
