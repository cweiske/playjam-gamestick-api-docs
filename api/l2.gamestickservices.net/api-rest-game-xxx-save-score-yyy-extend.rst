=========================================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/save-score/yyy/extend/view.json``
=========================================================================================

Store points in the game-specific leaderboard


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/save-score/yyy/extend/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``yyy``
    Score/points (integer)
  ``zzz``
    Session ID


HTTP response
=============
Must be on a single line.

Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-save-score-yyy-extend.response.json
   :code:

Real response rescued from a GameStick:

.. include:: api-rest-game-xxx-save-score-yyy-extend.real-response1.pretty.json
   :code:
