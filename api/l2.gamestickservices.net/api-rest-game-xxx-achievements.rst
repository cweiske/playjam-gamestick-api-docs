================================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/achievements/view.json``
================================================================================

Fetch achievements for a single game.

Accessed from the GameStick profile view when selecting a game.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/achievements/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``zzz``
    Session ID


HTTP response
=============
Must be on a single line.

Used achievement properties seem to be ``id``, ``achievementName``, ``description``,
``fileUrl`` and ``isCurrentUserOwner``.

Images have an aspect ratio of 7:4, around 177x101px.

Grayscale ``.png`` images are not supported; they must be full-colored ones.

Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-achievements.response.json
   :code:
