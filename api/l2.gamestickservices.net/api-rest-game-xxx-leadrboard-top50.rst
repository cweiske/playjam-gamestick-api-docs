====================================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/leadrboard/top50/view.json``
====================================================================================

Fetch the first 50 entries in the game's high score list.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/leadrboard/top50/view.json;jsessionid=zzz``
    The missing ``e`` is intentional.
  ``xxx``
    UUID of the game
  ``yyy``
    Score/points (integer)
  ``zzz``
    Session ID


HTTP response
=============
Must be on a single line.

FIXME: Unclear what values are expected in ``avatarId``.

Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-leadrboard-top50.response.json
   :code:

Real response rescued from a GameStick:

.. include:: api-rest-game-xxx-leadrboard-top50.real-response1.pretty.json
   :code:
