===================================================================================
``GET http://l2.gamestickservices.net/api/rest/user/achievement/summary/view.json``
===================================================================================

Get an achievement summary with statistics of games and their achievement numbers.

HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/user/achievement/summary/view.json;jsessionid=zzz``

  ``zzz``
    Session ID

Example::

  GET http://l2.gamestickservices.net/api/rest/user/achievement/summary/view.json;jsessionid=sacdbda09185c


HTTP response
=============

Status code
  ``200 OK``

Example:

.. include:: api-rest-user-achievement-summary.response-success.json
   :code:

Property notes:

``body.gameAchievementSummary.gameId``
  Probably ``body.config.apps[].id`` from
  ``POST http://l2.gamestickservices.net/api/rest/connect/stick/stick/xxx/view.json``.

  Type: int

``body.gameAchievementSummary.gameName``
  Title of the game

  Type: string

``body.gameAchievementSummary.gameIdentifcation``
  Proably UUID of the game release.

  Yes, there is a typo in that property name.

  Type: string

  Example: ``0cfc156cdca036835aa5414bafb610b8``

``body.gameAchievementSummary.lastAchievementImageUrl``
  FIXME: Seems not to be used.

  Best size: FIXME

  Type: string

``body.gameAchievementSummary.totalNumberofAchievements``
  Number of possible achievements in the game

  Yes, the "o" in "of" is inconsistently lowercase.

  Type: int

``body.gameAchievementSummary.numberOfPlayerUnlockedAchievements``
  How many the player unlocked

  Type: int
