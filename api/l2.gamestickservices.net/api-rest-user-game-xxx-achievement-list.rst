=========================================================================================
``GET http://l2.gamestickservices.net/api/rest/user/game/xxx/achievement/list/view.json``
=========================================================================================

Fetch all achievements for a single game including the info if the user has unlocked them.

Used by games to check which achievements have already been unlocked by the user.

Known usage:

- When "Bloo Kid" starts


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/user/game/xxx/achievement/list/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``zzz``
    Session ID


HTTP response
=============
The response must be on one single line!

At least "Bloo Kid" only cares about the ID.
The other properties must exist (JSON parsing fails otherwise),
but their values are not used.

The ``id`` values are known by the server and had to be registered in
the PlayJam Publishing Portal.

``isCurrentUserOwner`` must be an integer, not a boolean.

Status code
  ``200 OK``

Example

.. include:: api-rest-user-game-xxx-achievement-list.response.json
   :code:

"Boulder Dash" does not want the ``success`` property but requires ``body``
to be an array listing the achievements - which contradicts the requirements
by Bloo Kid and the GameStick console API:

.. include:: api-rest-user-game-xxx-achievement-list.response-bd.json
   :code:

Known achievement IDs:

``461``
  Bloo Kid: Triple Hit

All known IDs are listed in the game data files.
