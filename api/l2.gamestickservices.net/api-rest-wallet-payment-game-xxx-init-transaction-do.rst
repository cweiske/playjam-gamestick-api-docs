======================================================================================================
``GET http://l2.gamestickservices.net/api/rest/wallet/payment/game/xxx/init-transaction/do/view.json``
======================================================================================================

When clicking "buy" in the game details screen.


Known to be used in firmware versions:

- 2058


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/wallet/payment/game/xxx/init-transaction/do/view.json;jsessionid=zzz``

  ``xxx``
    Game ID.

    Empty in firmware 2051.
  ``zzz``
    Session ID from the registration check
    ``api/rest/connect/stick/stick/xxx/view.json``.


HTTP response
=============
Everything must be on one single line!

FIXME

Dummy example::

  {
      "body": {
          "pageUrl": "xxx",
          "url": "yyy"
      }
  }

Only one of pageUrl and url is needed, depends on request
(if internal request name is ``webview_displaywebpage`` or ``webview_displayactivewebpage`` -> ``url``, otherwise ``pageUrl``).

This here should use pageUrl.


Usage
=====

- ``com.playjam.gamestick.DatabaseInterfaceService.apk``:
  ``com.playjam.gamestick.databaseinterfaceservice.DatabaseInterfaceService#ParsePurchaseItemResponse()``
