=========================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/state/view.json``
=========================================================================

Load the current game progress from the server (savegame).


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/state/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``zzz``
    Session ID


HTTP response
=============
Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-state.response.json
   :code:
