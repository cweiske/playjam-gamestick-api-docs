=========================================================================
``GET http://l2.gamestickservices.net/api/rest/player/profile/view.json``
=========================================================================

Fetch player information.

In firmware v2071 OOBE setup will not finish when the profile is not returned
properly.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``

  Firmware 0.0.53 used ``db.gamestickservices.net``
Path
  ``/api/rest/player/profile/view.json;jsessionid=xxx``

  ``xxx``
    Session ID from the registration check
    ``api/rest/connect/stick/stick/xxx/view.json``.

    Empty when not registered yet.
Headers
  ``User-Agent``
    Examples:

    - ``Dalvik/1.6.0 (Linux; U; Android 4.1.2; GameStick V1.0 Build/V1.03.04MX01_20130911)``
    - ``GameStick/1.0``
  ``Accept-Encoding``
    - ``gzip`` only when ``User-Agent`` is ``Dalvik/...``
    - ``identity`` when ``User-Agent`` is ``GameStick/1.0``


HTTP response
=============

Not registered yet
------------------
Status code
  ``200 OK``

FIXME


User has been registered
------------------------
Property notes:

``addr``
  IP-Address (probably of client)

  Type: string

``accessCount``
  FIXME

  Type: int

  Example: ``1``

``created``
  Type: int

  Example: ``1382005635322``

``lastaccessed``
  Type: int

  Example: ``1382005635738``

``sid``
  Session ID (same as in request URL)

  Type: string

  Example: ``79C9B23DBA2682FEDFD0231A9AA28312``

``time``
  Type: string

  Example: ``"1382005637299"``

``body``
  The actual profile data

  ``body.accountType``
    Type: string

    Known values:

    - ``MANUALY_DEFINED``
    - ``CONSUMER``

  ``body.achievementStatus``
    Type: object

    ``body.achievementStatus.lastAchievementGameName``
      Type: string | null

      Title of the game that the player unlocked the last achievement in.

      Mapping to the game data really happens via the game name, and not
      with a package name or UUID.

      The game's ``logo-1`` file is used in the profile overview page.

      ``null`` when no achievement gotten yet.

      Example: ``Bloo Kid``

    ``body.achievementStatus.numberOfAchievementsUnlocked``
      Type: int

      Sum of achievements unlocked in all games

      Example: ``23``

  ``body.avatarLargeUrl``
    Type: string

    Image with 400x400

  ``body.avatarSmallUrl``
    Type: string

    Image with 118x118px

  ``body.action``
    ``null`` in all known cases.

    Type: string

  ``body.balance``
    Type: object

    ``body.balance.amountOfMoneyLeft``
      Type: string

      Examples:

      - ``USD 0.00``
      - ``GBP 25.00``
    ``body.balance.transactions``
      Type: array

      ``body.balance.transactions[].amount``
        Type: string

        When buying a game. ``null`` when uploading money.

        Example: ``GBP 2.99``
      ``body.balance.transactions[].balance``
        Type: string

        Example: ``GBP 25.00``
      ``body.balance.transactions[].date``
        Type: string

        Must contain the string " - " (space dash space).
        Before: date, after: description

        Examples:

        - ``16/10/2013 - TOP UP:PREPAID_CARD``
        - ``19/10/2014 - TOP UP:PAYPAL`` - when uploading money via PayPal
        - ``12/10/2014 - Slingshot Racing`` - game bought
      ``body.balance.transactions[].description``
        Type: string

        ``null`` in all observed files
      ``body.balance.transactions[].source``
        Type: string

        Known values:

        - ``PAYPAL``
        - ``PREPAID_CARD``
        - ``WALLET``
      ``body.balance.transactions[].type``
        Type: string

        Known values:

        - ``CREDIT_WALLET``
        - ``GAME PRODUCT``

  ``body.currency``
    Three-letter currency code

    Known values:

    - ``GBP``
    - ``USD``
  ``body.dateJoined``
    Type: string

    Example: ``23/09/2014``
  ``body.dateOfBirth``
    Type: string

    Example: ``08/08/1984``
  ``body.email``
    Type: string
  ``body.founderFlag``
    Type: int

    The 25$ Kickstarter perk:

     FOUNDER TAG: Name Check + Reserve your username before launch
     + Get a founder tag next to that username.
  ``body.founderName``
    Type: string
  ``body.gamertag``
    Type: string

    Profile user name
  ``body.location``
    Type: string

    Two-letter uppercase country code

    Known values:

    - ``GB``
    - ``US``
  ``body.message``
    Type: string

    Does not seem to be used in 2071.
  ``body.minAge``
    Type: int

    Example: ``17``
  ``body.minAgeLabel``
    Type: string

    Example: ``17+``
  ``body.password``
    Password hash (probably for age change verification)

    Example: ``75381f9f2bd23d8b4a0dcb0ad7c364ff``
  ``body.securityLevel``
    Type: int

    Known values:

    - ``0``
    - ``1``
  ``body.success``
    Type: string

    Does not seem to be used in 2071.

.. include:: api-rest-player-profile.response-full.json
   :code:



Usage
=====
- ``com.playjam.Services.Database.ServiceCore#downloadProfile()``
