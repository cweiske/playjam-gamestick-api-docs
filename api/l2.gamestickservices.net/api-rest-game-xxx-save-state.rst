==============================================================================
``GET http://l2.gamestickservices.net/api/rest/game/xxx/save-state/view.json``
==============================================================================

Store the current game progress on the server (savegame).


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/xxx/save-state/view.json;jsessionid=zzz``

  ``xxx``
    UUID of the game
  ``zzz``
    Session ID
Query parameters
  ``?data=Rm9vIDIzIDQyCg%3D%3D``

  ``mydata``
    base64-encoded binary save state data (URL-encoded of course)


HTTP response
=============
Status code
  ``200 OK``

Example

.. include:: api-rest-game-xxx-save-state.response.json
   :code:
