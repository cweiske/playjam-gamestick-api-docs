=========================================================================================================
``GET http://l2.gamestickservices.net/api/rest/wallet/payment/topupwallet/init-transaction/do/view.json``
=========================================================================================================

Start adding credits to the wallet in the player profile ("Add credit").

The GameStick shows the webpage URL ``pageUrl`` in a window.

FIXME: Somehow the webpage must signal that adding credits worked.


HTTP request
============
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/wallet/payment/topupwallet/init-transaction/do/view.json;jsessionid=zzz``

  ``zzz``
    Session ID


HTTP response
=============
Status code
  ``200 OK``

.. include:: api-rest-wallet-payment-topupwallet-init-transaction-do.response-url.json
   :code:
