===========================================================================================
``GET http://l2.gamestickservices.net/api/rest/game/downloadedfreegame/xxx/true/view.json``
===========================================================================================

When a game has been downloaded free of charge.


HTTP request
============
Method
  ``GET``
Protocol
  ``http``
Host
  ``l2.gamestickservices.net``
Path
  ``/api/rest/game/downloadedfreegame/xxx/true/view.json;jsessionid=zzz``

  ``xxx``
    Game ID, e.g. ``d8a7bea559a34ba2a71b97f99de54884``

  ``zzz``
    Session ID
Headers
  ``User-Agent``
    ``Dalvik/1.6.0 (Linux; U; Android 4.1.2; GameStick V1.0 Build/V1.03.04MX01_20130911)``
  ``Connection``
    ``Keep-Alive``
  ``Accept-Encoding``
    ``gzip``


HTTP response
=============
FIXME
