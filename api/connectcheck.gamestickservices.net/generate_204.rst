==============================================================
``GET http://connectcheck.gamestickservices.net/generate_204``
==============================================================

Network availability ping via HTTP.

Firmware 2071 resolves the IP address for the hostname and
sends the HTTP request directly to the IP address without providing
a ``Host`` header.

At least firmware 0.0.53 sends the host name in the request.


HTTP request
============
Protocol
  ``http``
Host
  1. ``connectcheck.gamestickservices.com``
  2. ``connectcheck.gamestickservices.net``
  3. ``clients3.google.com``
Path
  ``generate_204``


HTTP response
=============

Success
-------
Status code
  ``204 No Content``



Usage
=====
- ``com.playjam.gamestick.WifiTools.ChecksFragment#http()``

  Only ``connectcheck.gamestickservices.net``

- ``com.playjam.gamestick.WifiService#ping()``

  Tries all three servers until it finds one that responds.
