=======================================================
``GET http://www.playjam.com/bundles/system/meta.json``
=======================================================

Fetch link to the URL of the latest Companion App configuration file.

Used by the companion app.


HTTP request
============
Protocol
  ``http``
Host
  ``www.playjam.com``
Path
  ``/bundles/system/meta.json``


HTTP response
=============
Property notes:

``timestamp``
  Type: long.
  Optional.

  ``0`` to disable Companion App bundle file download.

``bundleURL``
  Path to ``.zip`` file with configuration data.

Example:

.. include:: bundles-system-meta.json.response.json
   :code:



Usage
=====
- ``com.playjam.companionapp.service.CAServerService#fetchSystemBundle()``
