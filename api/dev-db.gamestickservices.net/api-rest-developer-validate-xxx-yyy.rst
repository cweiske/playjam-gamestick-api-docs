=========================================================================================
``GET http://dev-db.gamestickservices.net/api/rest/developer/validate/xxx/yyy/view.json``
=========================================================================================

Verify that the GameStick may switch to the developer firmware.


HTTP request
============
Protocol
  ``http``
Host
  ``dev-db.gamestickservices.net``
Path
  ``/api/rest/developer/validate/xxx/yyy/view.json;jsessionid=zzz``

  ``/api/rest/developer/validate/xxx/yyy/view.json``
    OOBE does not add the ``jsessionid`` parameter

  ``xxx``
    Hardware ID

    Example: ``ac:db:da:09:18:5c``
  ``yyy``
    Verification code entered by the user.

    7 characters long, all digits. Last digit is checksum calculated
    with the Damm algorithm.
  ``zzz``
    Session ID


HTTP response
=============
Standard response, with ``success`` set to ``true``.

.. include:: api-rest-developer-validate-xxx-yyy.response.json
   :code:
