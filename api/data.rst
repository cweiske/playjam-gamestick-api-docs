===================
Common data formats
===================

JSON responses share some properties.

``date``
  Dates have format ``dd/MM/yyyy`` - e.g. 23/12/2013.
``time``
  Time since 01.01.1970 in milliseconds (unix timestamp * 1000)

JSON responses MUST be condensed to a single line.
