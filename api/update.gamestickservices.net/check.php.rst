======================================================
``POST http://update.gamestickservices.net/check.php``
======================================================

Check if a new firmware update is available.

Three hosts are checked one after another.
When ``update.gamestickservices.com`` is not available,
it may take up to two minutes until the ``.net`` host is tried.


HTTP request
============
Protocol
  ``http``
Host
  Three hosts are checked in firmware 2071:

  1. ``update.gamestickservices.com``
  2. ``update.gamestickservices.net``
  3. ``54.215.8.117``
Path
  ``check.php``
Headers
  ``Content-Type``
    ``application/x-www-form-urlencoded``
POST parameters
  ``v``
    JSON-encoded hardware information (version 0.0.53)::

        {
            "hwid": "ac:db:da:09:18:5c",
            "major": 0,
            "minor": 0,
            "revision": 53,
            "platform": 0
        }

    version 2049::

      {"platform":0,"revision":2049,"minor":9,"hwid":"ac:db:da:09:18:5c","major":0}


HTTP response
=============
JSON must be one a single line; parsing will fail otherwise.


Update available
----------------
Status code
  ``200 OK``

.. include:: check.php.response-update.json
   :code:


No update available
-------------------
Status code
  ``200 OK``

.. include:: check.php.response-no-update.json
   :code:



Usage
=====
- ``com.playjam.gamestick.WifiTools.ChecksFragment#doUpdateConnect()``
- ``com.playjam.UpdateService``



Update download process
=======================
Firmware updates are downloaded by a separate downloading process
in ``com.playjam.UpdateService``.

Updates are downloaded in small chunks that are combined to a single file
in the end.
Firmware updates are standard Android OTA ``.zip`` update files that
were renamed to ``.img`` by PlayJam. (``GameStick-Software-v2071.img``).

The download URL given in the HTTP reponse is appended with ``&i=`` plus
the chunk index number.

Two special chunk numbers exist:

``-2``
  Returns the total size of the download (plain text ``long`` value)
``-1``
  Returns the number of chunks (plain text, ``int``)

``-2`` is fetched first, ``-1`` second and then ``0``, ``1`` and so forth.

After download and chunk combination, the file is verified by standard Android
mechanism ``android.os.RecoverySystem.verifyPackage()``.

Information about the new firmware is written to ``/data/GameStickCache/update.info``.


Chunks
------
Generating chunk files:

1. The firmware update file must be split into files of 102400 bytes.
2. Each part must be XORed with 91 (hex ``5b``, binary ``1011011``).
3. The binary SHA1 sum of the XORed part file must pre prepended before the data

Having a single chunk file only should work, too.
