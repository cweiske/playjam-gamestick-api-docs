================================================================================
``GET http://db.gamestickservices.com/api/rest/joystick/bindings/xxx/view.json``
================================================================================

Fetch controller mapping for a game.

Games had to request input maps themselves by sending the
``com.playjam.InputService.Remap`` intent with the game ID.

They could also send a keymap XML via the ``com.playjam.InputService.RemapXML``
intent, and the ``com.playjam.InputService`` would then take care
of mapping the keys.

Unfortunately, InputService hard-codes the ``db.gamestickservices.com`` domain
and does not use the ``.net`` one.

Games known to request the input map file:

- Sela the Space Pirate, id 171


HTTP request
============
Protocol
  ``http``
Host
  ``db.gamestickservices.com``
Path
  ``/api/rest/joystick/bindings/xxx/view.json``

  ``xxx``
    UUID of the game


HTTP response
=============
Must be on a single line.


Status code
  ``200 OK``

Example

.. include:: api-rest-joystick-bindings-xxx.response.json
   :code:


Keymap file
===========

.. include:: api-rest-joystick-bindings-xxx.keymap.xml
   :code:
