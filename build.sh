#!/bin/sh
rst2html5\
 --stylesheet-path=styles.css --embed-stylesheet\
 --title="PlayJam GameStick server API documentation"\
 README.rst gamestick-api-docs.htm

# add current date to rendered docs
sed -i "s/##UPDATE##/`date -Is`/" gamestick-api-docs.htm
